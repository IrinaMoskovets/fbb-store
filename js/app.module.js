'use strict';

angular.module('BookApp', [
    'ngRoute',
    'core',
    'bookDetail',
    'bookList',
    'bookForm',
]);