'use strict';

angular.
    module('BookApp').
    config(['$locationProvider' ,'$routeProvider',
        function config($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');

            $routeProvider.
            when('/books', {
                template: '<book-list></book-list>'
            }).
            when('/books/:bookId', {
                template: '<book-detail></book-detail>'
            }).
            when('/books/form/:bookId', {
                template: '<book-form></book-form>'
            }).
            when('/about', {
                templateUrl: 'about/about.html'
            }).
            otherwise('/books');
        }
    ]);