'use strict';

angular.
    module('core.book').
    factory('Book', ['$resource',
        function($resource) {
            return $resource('https://netology-fbb-store-api.herokuapp.com/book/:bookId', {}, {
                query: {
                    method: 'GET',
                    params: {bookId: ''},
                    isArray: true
                }
            });
        }
    ]);

angular.
    module('core.book').
    factory('Form', ['$resource',
        function($resource) {
            return $resource('https://netology-fbb-store-api.herokuapp.com/order/delivery', {}, {
                query: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);

angular.
    module('core.book').
    factory('Payment', ['$resource',
        function($resource) {
            return $resource('https://netology-fbb-store-api.herokuapp.com/order/payment', {}, {
                query: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);

