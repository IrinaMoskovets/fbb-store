'use strict';

// Register `bookList` component, along with its associated controller and template
angular.
    module('bookList').
    component('bookList', {
        templateUrl: 'book-list/book-list.template.html',
        controller: ['$scope', '$sce', 'Book',
            function BookListController($scope, $sce, Book) {
                this.books = Book.query();
                $scope.$sce = $sce;

                $scope.numLimit = 4;
                $scope.Limit = function() {
                    $scope.numLimit = $scope.numLimit + 4;

                };
            }
        ]
    });
