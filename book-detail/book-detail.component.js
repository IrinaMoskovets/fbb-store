'use strict';

// Register `bookDetail` component, along with its associated controller and template
angular.
module('bookDetail').
component('bookDetail', {
    templateUrl: 'book-detail/book-detail.template.html',
    controller: ['$scope', '$sce', '$routeParams', 'Book', //"$element", "$window", "MouseWatch",
        function BookDetailController($scope, $sce, $routeParams, Book) {
            this.book = Book.get({bookId: $routeParams.bookId});
            $scope.$sce = $sce;
        }
    ]
});