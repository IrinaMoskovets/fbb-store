module.exports = function(grunt) {

	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),
		sass: {
		     dist: {
				 files: {
					'css/main.css': 'styles/main.scss'
				}
			}
		},
		ftp_push: {
		    demo: {
		    	options: {
		    		authKey: 'netology',
		    		host: 'university.netology.ru',
		    		dest: '/fbb-store/',
		    		port: 21
		    	},
		    	files: [{
		    		expand: true,
		    		cwd: '.',
		    		src: [
		    		      'index.html',
		    		      'css/main.css',
						  'js/core/core.module.js',
						  'js/core/book/book.module.js',
						  'js/core/book/book.service.js',
						  'js/app.module.js',
						  'js/app.config.js',
						  'img/*',
						  'font/*',
						  'book-list/*',
						  'book-detail/*',
						  'about/*',
						  'node_modules/normalize-css/normalize.css',
						  'node_modules/angular/angular.js',
						  'node_modules/angular-resource/angular-resource.js',
						  'node_modules/angular-route/angular-route.js'
		    		]
		        }]
		    }
		 }
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-ftp-push');
	
	grunt.registerTask('default', ['sass', 'ftp_push']);
	grunt.registerTask('start', ['sass']);

};