'use strict';

// Register `bookForm` component, along with its associated controller and template
angular.
module('bookForm').
component('bookForm', {
    templateUrl: 'form/book-form.template.html',
    controller: ['$scope', '$sce', '$routeParams', 'Book', 'Form', 'Payment', '$http',
        function BookFormController($scope, $sce, $routeParams, Book, Form, Payment, $http) {
            this.book = Book.get({bookId: $routeParams.bookId});
            $scope.$sce = $sce;

            this.deliveryMode = Form.query();
            this.paymentMode = Payment.query();



            $scope.method = 'POST';
            $scope.url = 'https://netology-fbb-store-api.herokuapp.com/order';

            $scope.submitOrder = function(order, book) {
                $scope.order2 = {
                    'manager': 'vikky-sun16@yandex.ru',
                    'book': book,
                    'name': order.name,
                    'phone': order.phone,
                    'email': order.email,
                    'comment': order.comment,
                    'delivery': {
                        'id': order.delivery.id,
                        'address': order.address
                    },
                    'payment': {
                        'id': order.payment.id,
                        'currency': order.payment.currency
                    }
                };

                $scope.code = null;
                $scope.response = null;

                $http({method: $scope.method, url: $scope.url, data: $scope.order2}).
                success(function(data, status) {
                    $scope.status = status;
                    $scope.data = "<p class='success'>Заказ на книгу успешно оформлен.<br>Спасибо что спасли книгу от сжигания в печи.</p>";
                }).
                error(function(data, status) {
                    $scope.data = "<p class='error'>Произошла ошибка. Попроуйте еще раз!</p>";
                    $scope.status = status;
                });
            };
        }
    ]
});