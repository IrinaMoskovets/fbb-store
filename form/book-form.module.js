'use strict';

// Define the `bookForm` module
angular.module('bookForm', [
    'ngRoute',
    'core.book'
]);

